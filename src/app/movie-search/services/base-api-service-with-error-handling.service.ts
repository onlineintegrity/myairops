import {HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {throwError, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export abstract class BaseApiServiceWithErrorHandling {

  errorHandler(error: HttpErrorResponse): Observable<any> {
    let errMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      errMessage = `Err: ${error.error.message}`;
    }
    else {
      errMessage = `Err: ${error.status} ${error.message}`;
    }
    return throwError(errMessage);
  }
}
