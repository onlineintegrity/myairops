import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {BaseApiServiceWithErrorHandling} from './base-api-service-with-error-handling.service';

@Injectable({
  providedIn: 'root',
})
export class TranslationService extends BaseApiServiceWithErrorHandling {

  private translationEndpoint = 'https://api.funtranslations.com/translate/cockney.json';

  constructor(private readonly http: HttpClient) {
    super();
  }

  sendTranslationRequest(text: string): Observable<any> {
    const params = new HttpParams()
      .set('text', text);
    return this.http.get(this.translationEndpoint, {params}).pipe(catchError(this.errorHandler));
  }
}
