import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {BaseApiServiceWithErrorHandling} from './base-api-service-with-error-handling.service';

@Injectable({
  providedIn: 'root',
})
export class MovieSearchService extends BaseApiServiceWithErrorHandling {

  private searchEndpoint = 'https://api.themoviedb.org/3/search/movie';
  private apiKey = '60ccf3adb1f3829af422add0c923b667';

  constructor(private readonly http: HttpClient) {
    super();
  }

  sendSearchRequest(searchString: string): Observable<any> {
    const params = new HttpParams()
      .set('language', 'en-GB')
      .set('api_key', this.apiKey)
      .set('query', searchString);
    return this.http.get(this.searchEndpoint, {params}).pipe(catchError(this.errorHandler));
  }
}
