import {Component, Input} from '@angular/core';
import {finalize} from 'rxjs/operators';
import {TranslationService} from '../../services/translation.service';

@Component({
  selector: 'app-translator',
  templateUrl: './translator.component.html',
})
export class TranslatorComponent {

  @Input() copy: string;
  translation: string | null = null;
  loading = false;
  serviceError = false;
  active = false;

  constructor(private readonly translationService: TranslationService) {
  }

  switchVersion(): void {
    if (this.translation === null) {
      this.translate();
    }
    this.active = !this.active;
  }

  translate(): void {
    if (!this.copy) {
      this.translation = '';
    }else{
      this.loading = true;
      this.translationService.sendTranslationRequest(this.copy).pipe(
        finalize(() => {
          this.loading = false;
        }))
          .subscribe((data: any) => {
            console.log(data);
            this.translation = data.contents.translated;
          }, (err: any) => {
            this.serviceError = true;
            this.translation = '';
          });
    }
  }

}
