import {ComponentFixture, TestBed} from '@angular/core/testing';
import {BehaviorSubject, throwError} from 'rxjs';
import {TranslationService} from '../../services/translation.service';
import {TranslatorComponent} from './translator.component';

describe('TranslatorComponent', () => {
  let component: TranslatorComponent;
  let fixture: ComponentFixture<TranslatorComponent>;
  const mockTranslationService = jasmine.createSpyObj(['sendTranslationRequest']);
  const mockResp: any = {
    contents: {
      translated: 'cor blimey guvnor',
    },
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TranslatorComponent],
      providers: [
        {provide: TranslationService, useValue: mockTranslationService},
      ],
    });
    fixture = TestBed.createComponent(TranslatorComponent);
    component = fixture.componentInstance;
    mockTranslationService.sendTranslationRequest.calls.reset();
    component.serviceError = false;
    component.active = false;
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('switchVersion()', () => {
    it('should switch active and call translate if it has not been called already', () => {
      mockTranslationService.sendTranslationRequest.and.returnValue(new BehaviorSubject(mockResp).asObservable());
      component.translation = null;
      component.copy = 'I say sir';
      component.switchVersion();
      expect(mockTranslationService.sendTranslationRequest).toHaveBeenCalledWith('I say sir');
      expect(component.active).toBeTruthy();
    });

    it('should switch active and not call translate if it has been called already', () => {
      mockTranslationService.sendTranslationRequest.and.returnValue(new BehaviorSubject(mockResp).asObservable());
      component.active = true;
      component.translation = 'Lord love a duck';
      component.copy = 'I say sir';
      component.switchVersion();
      expect(mockTranslationService.sendTranslationRequest).not.toHaveBeenCalledWith('I say sir');
      expect(component.active).toBeFalsy();
    });
  });

  describe('translate()', () => {
    it('should do the translation if there is an input string', () => {
      mockTranslationService.sendTranslationRequest.and.returnValue(new BehaviorSubject(mockResp).asObservable());
      component.copy = 'I say sir';
      component.translate();
      expect(component.serviceError).toBeFalsy();
      expect(mockTranslationService.sendTranslationRequest).toHaveBeenCalledWith('I say sir');
      expect(component.translation).toEqual('cor blimey guvnor');
    });

    it('should skip the translation if there is a falsy input string', () => {
      mockTranslationService.sendTranslationRequest.and.returnValue(new BehaviorSubject(mockResp).asObservable());
      component.copy = '';
      component.translate();
      expect(component.serviceError).toBeFalsy();
      expect(mockTranslationService.sendTranslationRequest).not.toHaveBeenCalled();
      expect(component.translation).toEqual('');
    });

    it('should do a translation search and handle errors', () => {
      const fakeError = new Error('Fake error');
      mockTranslationService.sendTranslationRequest.and.returnValue(throwError(fakeError));
      component.copy = 'I say sir';
      component.translate();
      expect(component.serviceError).toBeTruthy();
      expect(mockTranslationService.sendTranslationRequest).toHaveBeenCalledWith('I say sir');
      expect(component.translation).toEqual('');
    });
  });

});
