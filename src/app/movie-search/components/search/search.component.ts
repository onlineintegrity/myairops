import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators, AbstractControl} from '@angular/forms';
import {finalize} from 'rxjs/operators';
import {Movies} from '../../models/movies.model';
import {MovieSearchService} from '../../services/movie-search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit {

  constructor(private readonly movieSearchService: MovieSearchService) {
  }

  searchForm: FormGroup;
  loading = false;
  serviceError: string | null = null;
  movies: Movies | null = null;

  get searchControl(): AbstractControl | null {
    return this.searchForm?.get('searchControl');
  }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm(): void {
    this.searchForm = new FormGroup({
      searchControl: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
    });
  }

  doSearch(): void {
    if (this.searchForm.valid) {
      this.serviceError = null;
      this.loading = true;
      this.movies = null;
      this.searchControl?.markAsPristine();
      this.movieSearchService.sendSearchRequest(this.searchControl?.value).pipe(
        finalize(() => {
          this.loading = false;
        }))
          .subscribe((data: any) => {
            this.movies = new Movies(data);
          }, (err: any) => {
            this.serviceError = err;
          });
    }
  }

}
