import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {BehaviorSubject, throwError} from 'rxjs';
import {Movies} from '../../models/movies.model';
import {MovieSearchService} from '../../services/movie-search.service';
import {SearchComponent} from './search.component';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let formBuilder: FormBuilder;
  const mockMovieSearchService = jasmine.createSpyObj(['sendSearchRequest']);
  const mockResp: any = {
    results: [
      {title: 'romantic movie', release_date: '2015-05-08', overview: 'girl meets boy and falls in love'},
      {title: 'horror movie', release_date: '2018-06-12', overview: 'girl meets boy and kills him'},
      {title: 'kids movie', release_date: '2011-05-10', overview: 'girl meets stray dog and saves condemned charity project'},
    ],
    total_results: 3,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SearchComponent],
      providers: [
        {provide: MovieSearchService, useValue: mockMovieSearchService},
        FormBuilder,
      ],
    });
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    formBuilder = TestBed.inject(FormBuilder);
    mockMovieSearchService.sendSearchRequest.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  describe('ngOnInit()', () => {
    it('should build the form', () => {
      component.ngOnInit();
      expect(component.searchForm).toBeTruthy();
    });
  });

  describe('doSearch()', () => {
    it('should do the search if the form is valid', () => {
      const testMovies = new Movies(mockResp);
      mockMovieSearchService.sendSearchRequest.and.returnValue(new BehaviorSubject(mockResp).asObservable());
      component.ngOnInit();
      component.searchControl?.setValue('Bambi');
      component.doSearch();

      expect(component.searchForm.valid).toBeTruthy();
      expect(component.serviceError).toBeNull();
      expect(mockMovieSearchService.sendSearchRequest).toHaveBeenCalledWith('Bambi');
      expect(component.movies).toEqual(testMovies);
    });

    it('should skip the search if the form is invalid', () => {
      mockMovieSearchService.sendSearchRequest.and.returnValue(new BehaviorSubject(mockResp).asObservable());
      component.ngOnInit();
      component.searchControl?.setValue('ET');
      component.doSearch();

      expect(component.searchForm.valid).toBeFalsy();
      expect(component.serviceError).toBeNull();
      expect(mockMovieSearchService.sendSearchRequest).not.toHaveBeenCalled();
    });

    it('should do a movie search and handle errors', () => {
      const fakeError = new Error('Fake error');
      mockMovieSearchService.sendSearchRequest.and.returnValue(throwError(fakeError));
      component.ngOnInit();
      component.searchControl?.setValue('Grease');
      component.doSearch();
      expect(mockMovieSearchService.sendSearchRequest).toHaveBeenCalledWith('Grease');
      expect(component.movies).toBeNull();
      expect(component.serviceError).toBeTruthy();
    });
  });

});
