import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {SearchComponent} from './components/search/search.component';
import {TranslatorComponent} from './components/translator/translator.component';

@NgModule({
  declarations: [
    SearchComponent,
    TranslatorComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [
    SearchComponent,
    TranslatorComponent
  ]
})
export class MovieSearchModule {
}
