import {Movies} from './movies.model';

describe('MovieModel', () => {
  let movies: Movies;
  const respMovies: any = {
    results: [
      {title: 'romantic movie', release_date: '2015-05-08', overview: 'girl meets boy and falls in love'},
      {title: 'horror movie', release_date: '2018-06-12', overview: 'girl meets boy and kills him'},
      {title: 'kids movie', release_date: '2011-05-10', overview: 'girl meets stray dog and saves condemned charity project'},
    ],
    total_results: 8,
  };

  beforeEach(() => {
    movies = new Movies(respMovies);
  });

  it('should construct from a movie db response', () => {
    expect(movies.movies[0].name).toEqual('romantic movie');
    expect(movies.movies[1].dateStr).toEqual('2018-06-12');
    expect(movies.movies[1].year).toEqual(2018);
    expect(movies.movies[2].description).toEqual('girl meets stray dog and saves condemned charity project');
    expect(movies.count).toEqual(3);
    expect(movies.partialSet).toBeTruthy();
  });

  it('should tell us whether this is a partial set of results', () => {
    movies.trueCount = 3;
    expect(movies.partialSet).toBeFalsy();

    movies.trueCount = 13;
    expect(movies.partialSet).toBeTruthy();
  });

  it('should return integer 0 for count an empty set', () => {
    const noMoviesResp: any = {
      results: [],
      total_results: 0,
    };

    const noMovies = new Movies(noMoviesResp);
    expect(noMovies.count).toEqual(0);
    expect(noMovies.partialSet).toBeFalsy();
  });


});
