import {Movie} from './movie.model';

export class Movies {
  movies: Array<Movie>;
  trueCount = 0;

  constructor(movieDbArr: any) {
    this.movies = movieDbArr.results.map((movieSrc: any) => new Movie(movieSrc));
    this.trueCount = movieDbArr.total_results;
  }

  get count(): number {
    return this.movies?.length || 0;
  }

  get partialSet(): boolean {
    return this.trueCount > this.count;
  }
}


