export class Movie {
  name: string;
  dateStr: string;
  description: string;

  constructor(movieDbObj: any) {
    this.name = movieDbObj.title;
    this.dateStr = movieDbObj.release_date;
    this.description = movieDbObj.overview;
  }

  get year(): number | null {
    const movieReleaseDate = new Date(this.dateStr);
    return movieReleaseDate.getFullYear() || null;
  }

}
