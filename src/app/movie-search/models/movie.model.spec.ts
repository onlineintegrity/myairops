import {Movie} from './movie.model';

describe('MovieModel', () => {
  let movie: Movie;
  const respMovie: any = {title: 'romantic movie', release_date: '2015-05-08', overview: 'girl meets boy and falls in love'};

  beforeEach(() => {
    movie = new Movie(respMovie);
  });

  it('should construct from a movie db response', () => {
    expect(movie.name).toEqual('romantic movie');
    expect(movie.dateStr).toEqual('2015-05-08');
    expect(movie.description).toEqual('girl meets boy and falls in love');
  });

  it('should return the year for a sane date', () => {
    expect(movie.year).toEqual(2015);
  });

  it('should return null year for bad date', () => {
    movie.dateStr = '';
    expect(movie.year).toBeNull();
  });


});
