# Myairops

23/04/2021

Simon Walter for myairops

simon@onlineintegrity.net

07974344971

## Task

In Angular, we’d like you to build an application that satisfies the following

Submits search text to https://developers.themoviedb.org/3 (API Key you can use: 60ccf3adb1f3829af422add0c923b667) and returns the name, year and description of anything that matches that text

Takes the movie description and submits that to a Yoda translator -  https://funtranslations.com/api/yoda

The application should show the relevant movie information with the translation of the movie’s description passed through the Yoda Translator.

Please include a README.md

## Added Dependencies

Bootstrap and FontAwesome Free

Note: NPM install of FontAwesome Free seems to be broken or suspended. Have included it clunky old-school.

## Some notes and queries

I have not tested this app on any platform other than chromium browsers on a Linux desktop - using developer tools device emulation to check responsiveness.  

MovieDB returns paged results - I have added reference to that - but no mechanism to advance through the pages. With more time I would have created a reusable paging component - hopefully it is obvious that that would have posed few problems!

With more time and a real project - I would have made (for instance the search form and results list) more reusable components. 

I don't normally add unit tests for simple services - that is what E2E tests should be doing (IMO)

I have leaned fairly heavily on Bootstrap for quick and easy styling - my sass would be more structured and modular in normal circumstances.

FunTranslator has a throttle of 6 queries and hour - which ruled out auto translating each result. I have added a fairly crude handler for that restriction which just assumes that any error from those calls are due to throttling. Given more time I would have added specific error handling for that scenario.

This page is for test purposes - in the real world (given its 'fun' functionality) I would have given it more quirk. I thought maybe some pictures of pearly kings and queens dancing the conga!

The project is a vanilla latest angular cli new project build. The generic readme for that is below...

# Standard Angular Project Readme... 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.10.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
